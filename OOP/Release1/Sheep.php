<?php

class Animal
{

    public $sheep;
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($nama)
    {
        $this->sheep = $nama;
    }
}

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
