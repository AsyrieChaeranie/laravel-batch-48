<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Please!</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <!-- <h3>Sign Up Form</h3> -->
    <form action="/regist" method="post">
        @csrf
        <label for="fname">First name:</label>
        <br><input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label>
        <br><input type="text" id="lname" name="lname"><br><br>


        <label for="Gender">Gender:</label><br>
        <input type="radio" id="html" name="Gender" value="Male">
        <label for="html">Male</label><br>
        <input type="radio" id="css" name="Gender" value="Female">
        <label for="css">Female</label><br>
        <input type="radio" id="css" name="Gender" value="Other">
        <label for="javascript">Other</label><br><br>

        <label for="Nationality">Nationality:</label><br>
        <select name="Nationality" id="Nationality">
            <option value="Nationality1">Indoesian</option>
            <option value="Nationality2">Singaporean</option>
            <option value="Nationality3">Malaysian</option>
            <option value="Nationality4">Australian</option>
        </select><br><br>


        <label for="Language">Language Spoken:</label><br>
        <input type="checkbox" id="Language1" name="Language" value="Bahasa">
        <label for="Language"> Bahasa</label><br>
        <input type="checkbox" id="Language2" name="Language2" value="English">
        <label for="Language2"> English</label><br>
        <input type="checkbox" id="Language3" name="Language3" value="Other">
        <label for="Language3"> Other</label><br>

        <p><label for="Bio">Bio:</label></p>
        <textarea id="Bio" name="Bio" rows="4" cols="50"></textarea>
        <br><br>

        <input type="submit" value="Submit">
    </form>
</body>

</html>